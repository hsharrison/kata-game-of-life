"""Unit tests for gameoflife.increment_state."""
import pytest

from gameoflife import increment_state


def test_bad_data():
    """If the row lengths aren't identical, a TypeError should be raised."""
    with pytest.raises(TypeError):
        increment_state([[1], [1, 2]])


def test_underpopulation_rule_middle():
    """If a living cell in the middle has fewer than two neighbors, it should die."""
    initial_state = [
        [False, False, False],
        [False, True, False],
        [False, False, False],
    ]

    next_state = increment_state(initial_state)
    assert not next_state[1][1]


def test_underpopulation_rule_edge():
    """If a living cell on an edge has fewer than two neighbors, it should die."""
    initial_state = [
        [False, False, False],
        [False, True, True],
        [False, False, False],
    ]

    next_state = increment_state(initial_state)
    assert not next_state[1][2]


def test_underpopulation_rule_corner():
    """If a living cell on a corner has fewer than two neighbors, it should die."""
    initial_state = [
        [False, False, True],
        [False, False, True],
        [False, False, False],
    ]

    next_state = increment_state(initial_state)
    assert not next_state[0][2]


def test_stable_cells_middle():
    """If a living cell in the middle has two or three live neighbors, it should survive."""
    initial_state = [
        [False, False, True],
        [False, True, True],
        [False, False, False],
    ]
    next_state = increment_state(initial_state)
    assert next_state[1][1]


def test_stable_cells_edge():
    """If a living cell on an edge has two or three live neighbors, it should survive."""
    initial_state = [
        [False, False, True],
        [False, True, True],
        [False, False, True],
    ]
    next_state = increment_state(initial_state)
    assert next_state[1][2]


def test_stable_cells_middle_corner():
    """If a living cell in a corner has two or three live neighbors, it should survive."""
    initial_state = [
        [False, False, True],
        [False, True, True],
        [False, False, False],
    ]
    next_state = increment_state(initial_state)
    assert next_state[0][2]


def test_overpopulation_rule_middle():
    """If a living cell in the middle has more than three live neighbors, it should die."""
    initial_state = [
        [True, True, False],
        [True, True, False],
        [True, False, False],
    ]
    next_state = increment_state(initial_state)
    assert not next_state[1][1]


def test_overpopulation_rule_edge():
    """If a living cell on the edge has more than three live neighbors, it should die."""
    initial_state = [
        [True, True, False],
        [True, True, False],
        [True, False, False],
    ]
    next_state = increment_state(initial_state)
    assert not next_state[1][0]


def test_reproduction_rule_middle():
    """If a dead cell in the middle has exactly three live neighbors, it should become alive."""
    initial_state = [
        [True, True, False],
        [False, False, False],
        [True, False, False],
    ]
    next_state = increment_state(initial_state)
    assert next_state[1][1]


def test_reproduction_rule_edge():
    """If a dead cell on an edge has exactly three live neighbors, it should become alive."""
    initial_state = [
        [True, True, False],
        [False, False, False],
        [True, False, False],
    ]
    next_state = increment_state(initial_state)
    assert next_state[1][0]


def test_reproduction_rule_corner():
    """If a dead cell in a corner has exactly three live neighbors, it should become alive."""
    initial_state = [
        [False, True, False],
        [True, True, False],
        [True, False, False],
    ]
    next_state = increment_state(initial_state)
    assert next_state[0][0]


def test_stay_dead_middle():
    """If a dead cell in the middle has two live neighbors, it should stay dead."""
    initial_state = [
        [True, True, False],
        [False, False, False],
        [False, False, False],
    ]
    next_state = increment_state(initial_state)
    assert not next_state[1][1]


def test_stay_dead_edge():
    """If a dead cell on an edge has two live neighbors, it should stay dead."""
    initial_state = [
        [True, True, False],
        [False, False, False],
        [False, False, False],
    ]
    next_state = increment_state(initial_state)
    assert not next_state[1][0]


def test_stay_dead_corner():
    """If a dead cell in a corner has two live neighbors, it should stay dead."""
    initial_state = [
        [False, True, False],
        [True, False, False],
        [False, False, False],
    ]
    next_state = increment_state(initial_state)
    assert not next_state[0][0]
